# coding=utf-8
import logging

from utils.Message import Message
from utils.Antena import Antena
from utils.Stats import Stats
from utils.Probability import get_probability, choose_antenas

""" 
Init del Log i els objectes que necesitam pel
satel·lit, aixi com el posar en funcionament el 
metode per anar recollint les probabilitats.
"""
logging.basicConfig(level=logging.INFO,format='[%(levelname)s] [%(name)s] %(message)s',)

handler = logging.FileHandler('server.log')
handler.setLevel(logging.INFO)

# create a logging format
formatter = logging.Formatter('[%(levelname)s] [%(name)s] %(message)s')
handler.setFormatter(formatter)

# add the handlers to the logger
logger = logging.getLogger('Server')
logger.setLevel(logging.INFO)
logger.addHandler(handler)

m = Message()
m.watch = 'server'

n_antenas = 8

stat = Stats()
get_probability()

for i in range(n_antenas):
    a = Antena(i)


def fails_handler(msg, ciclo):
    """ Actualització de l'estat de les antenes, es mira la sincronització
    i despres es mira si es rompen les antenes. Els estats es solapen.
    """
    n_synchro = msg['synchro']
    n_broken = msg['broken']

    synchro = choose_antenas(n_synchro, sincro=True)    # Antenes des sincronitzades
    broken = choose_antenas(n_broken, sincro=False)     # Antenes rompudes   

    # Posam l'estat de les antenes
    for antena in synchro:
        m.use = 'antena_{}'.format(antena['id'])
        m.send(dict(pid=antena['id'], type='set_status', per=n_synchro, malfunction=antena['fucked'], ciclo=ciclo), priority=1)

    for antena in broken:
        m.use = 'antena_{}'.format(antena['id'])
        m.send(dict(pid=antena['id'], type='set_status', per=n_synchro, broken=antena['fucked'], ciclo=ciclo), priority=1)


def status_handler(msg, velocity):
    """ Actualitzam la velocitat depenent del nombre d'antenes
    rompudes i de les entenes desincronitzades
    """
    penaltys = 0
    status = msg['status']

    for stat in status:
        if status[stat]['broken']:
            penaltys += 1

    cruel = 9
    for penalty in range(penaltys):
        cruel *= 1.15

    for stat in status:
        if status[stat]['malfunction']:
            velocity -= cruel

    return velocity

if __name__ == '__main__':
    """ Bucle principal a on es va rebent els missatges.
    Primer es calcula totes les posibles fallades i despres
    es recullen totes les dades anteriorment generades
    per recalcular la velocitat actual.
    """
    velocity = 300
    km = 0
    ciclo = 1

    while velocity > 0:
        msg_received = m.receive()

        if msg_received['type'] == 'fails':
            fails_handler(msg_received, ciclo)
            logger.info('[{}] Velocidad: {} Km/h'.format(ciclo, velocity))
            km = km + velocity
            logger.info('[{}] Kilometros recorridos: {} Km'.format(ciclo, km))
            m.use = 'stat'
            m.send(dict(type='status_needed'), priority=1)

            ciclo += 1
        elif msg_received['type'] == 'total_status':
            velocity = status_handler(msg_received, velocity)
