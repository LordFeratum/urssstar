import threading
import logging
from math import exp
from utils.Message import Message


class Antena(threading.Thread):
    """ Especificacio duna antena del satellit.
    Cada antena sexecuta com un fil propi.
    """    
    def __init__(self, pid):
        """ Init del objecte Antenta i dels atributs basics 
        duna antena i dels missatges.
        """    
        threading.Thread.__init__(self)
        threading.Thread.daemon = True

        logging.basicConfig(level=logging.INFO, format='[%(levelname)s] [%(name)s] %(message)s'.format(pid),)
        
        handler = logging.FileHandler('antena_{}.log'.format(pid))
        handler.setLevel(logging.INFO)

        # create a logging format
        formatter = logging.Formatter('[%(levelname)s] [%(name)s] %(message)s'.format(pid))
        handler.setFormatter(formatter)

        # add the handlers to the logger
        self.logger = logging.getLogger('Antena {}'.format(pid))
        self.logger.setLevel(logging.INFO)
        self.logger.addHandler(handler)

        self._pid = pid
        self._period = 1
        self._mttf = 1450
        self._is_malfunction = False
        self._is_broken = False

        self._postman = Message()
        self._postman.use = 'server'
        self._postman.watch = 'antena_{}'.format(self._pid)

        self._continue = True

        self.logger.info('Inicializada'.format(self._pid))
        threading.Thread.start(self)

    def run(self):
        """ Metode principal del fil, depend del missatge que ens arribi
        anam a un o altra metde. 
        """ 
        while self._continue:
            msg = self.receive_msg()
            if msg['pid'] == self._pid:
                if msg['type'] == 'set_status':
                    self.set_status(msg)

                elif msg['type'] == 'get_status':
                    self.send_status()

    def send_status(self):
        """ Senvia dos atributs de lantena a lestat
        perque aquest pugui enviarli al servidor.
        """    
        msg = {'pid': self._pid,
               'type': 'antena_status',
               'status': {
                   'malfunction': self._is_malfunction,
                   'broken': self._is_broken
               }}

        self._postman.use = 'stat'
        self._postman.send(msg, priority=10)
        self._postman.use = 'server'

    def set_status(self, msg):
        """ Es pose lestat i el cicle de linstant 
        de temps del satellit i depenent daquest
        es posa al log de lantena en quin estat
        esta aquesta.
        """    
        self._period = msg['ciclo']

        if 'broken' in msg:
            if msg['broken']:
                availability = exp((-self._period / self._mttf)) * 100
                if round(float(msg['per']), 3) > availability:
                    self.logger.info(u'Se ha roto la antena en ciclo {}.'.format(self._period))
                    self._is_broken = True

        if 'malfunction' in msg:
            self._is_malfunction = msg['malfunction']
            if msg['malfunction']:
                self.logger.info(u'Se ha desincronizado la antena en ciclo {}.'.format(self._period))

    def receive_msg(self):  
        return self._postman.receive()
