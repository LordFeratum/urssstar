import random
import threading
import logging

from utils.Message import Message

logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] [%(name)s] %(message)s',)
handler = logging.FileHandler('probability.log')
handler.setLevel(logging.INFO)

# create a logging format
formatter = logging.Formatter('[%(levelname)s] [%(name)s] %(message)s')
handler.setFormatter(formatter)

# add the handlers to the logger
logger = logging.getLogger('Probability')
logger.setLevel(logging.INFO)
logger.addHandler(handler)


def get_probability():
    """ Fa un calcul probabilistic amb una funcio exponencial
    tant per la sincronitzacio dantenes com per la ruptura
    i despres retorna les probs al servidor per cada cas.
    """
    m = Message()
    m.use = 'server'

    types = ['synchro', 'broken']

    msg = {'type': 'fails',
           'synchro': 0,
           'broken': 0}

    for t in types:
        r = (random.expovariate(1) % 1) * 100
        msg.update({t: r})

    m.send(msg, priority=5)

    threading.Timer(0.2, get_probability).start()


def choose_antenas(prob, sincro):
    """ Es retornarnen les antenes que estan rompudes
    o en cas de sincronitzacio les que estan
    des sincronitzades.
    """
    n_antenas = 0
    
    # Tabla de fallades per l'URSS
    if prob <= 0.05:
        n_antenas = 8
    elif 0.05 < prob <= 0.09:
        n_antenas = 7
    elif 0.09 < prob <= 0.95:
        n_antenas = 6
    elif 0.95 < prob <= 1.2:
        n_antenas = 5
    elif 1.2 < prob <= 1.48:
        n_antenas = 4
    elif 1.48 < prob <= 2.24:
        n_antenas = 3
    elif 2.24 < prob <= 3.2:
        n_antenas = 2
    elif 3.2 < prob <= 5.75:
        n_antenas = 1

    if sincro:
        r = (random.expovariate(1) % 1) * 100
        if r <= 5:
            logger.info('Sincronizacion fallada por UC.')
        else:
            logger.info('Sincronizacion fallada por motor.')
        
        n_antenas -= 2  # Bizantins
        if n_antenas < 0:
            n_antenas = 0

    msg = []
    f_antenas = random.sample(range(0, 8), n_antenas)

    for antena in range(8):
        msg.append(dict(id=antena, fucked=(antena in f_antenas)))

    return msg
