import beanstalkc
import json


class Message(object):
    """ Clase que defineix els missatges i les seves
    funcionalitats per poder direccionarse en tuberies
    determinades per enviar i rebre, i les propies
    funcionalitats per enviar i rebre missatges. 
    """
    def __init__(self):
        self._beanstalk = beanstalkc.Connection(host='localhost', port=1300)

    @property
    def use(self):
        return self._beanstalk.using()

    @use.setter
    def use(self, pipe_value):
        self._beanstalk.use(pipe_value)

    @property
    def watch(self):
        return self._beanstalk.watching()

    @watch.setter
    def watch(self, watch_value):
        self._beanstalk.watch(watch_value)

    def send(self, dict_msg):
        self._beanstalk.put(json.dumps(dict_msg))

    def send(self, dict_msg, priority):
        self._beanstalk.put(json.dumps(dict_msg), priority=priority)

    def receive(self):
        job = self._beanstalk.reserve()
        msg = json.loads(job.body)
        job.delete()
        return msg
