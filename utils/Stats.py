__author__ = 'quique'

import threading
import logging
from utils.Message import Message


class Stats(threading.Thread):
    """ 
    Clase intermitja entre les antenes i el servidor.
    Quan el servidor necesita l'estat de totes les antenes,
    Stats s'encarrega d'enviar l'estat resumit de totes
    les antenes al servidor.
    """
    def __init__(self):
        """
         Init de l'objecte Stat. Init dels missatges.
        """    
        threading.Thread.__init__(self)
        threading.Thread.daemon = True

        logging.basicConfig(level=logging.info, format='[%(levelname)s] [%(name)s] %(message)s')
        
        handler = logging.FileHandler('stats.log')
        handler.setLevel(logging.INFO)

        # create a logging format
        formatter = logging.Formatter('[%(levelname)s] [%(name)s] %(message)s')
        handler.setFormatter(formatter)

        # add the handlers to the logger
        self.logger = logging.getLogger('Stats')
        self.logger.setLevel(logging.INFO)
        self.logger.addHandler(handler)
        
        self.logger.info('Iniciando stat.service')

        self._status = {}

        self._postdam = Message()
        self._postdam.use = 'server'
        self._postdam.watch = 'stat'

        self._continue = True

        threading.Thread.start(self)

    def run(self):
        """ 
        Metode principal del fil, dependent del missatge que
        ens arribi anam a un o altra metde.
        """
        while self._continue:
            msg = self._postdam.receive()
            #self.logger.debug('Recibido un mensaje de tipo {}.'.format(msg['type']))
            if msg['type'] == 'antena_status':
                self._add_status(msg)

            elif msg['type'] == 'status_needed':
                self._status_needed()

    def _add_status(self, msg):
        """ 
        Quan es rep l'estat de l'antena aquest es 
        resumeix en dos dels atributs. 
        """    
        self.logger.info('Recibido estado de Antena {}.'.format(msg['pid']))
        self._status.update({msg['pid']: msg['status']})

        if len(self._status.keys()) == 8:
            self.logger.info('Se tienen todos los datos. Mandando mensaje a Server ')
            nmsg = dict(type='total_status', status=self._status)
            self._status = {}
            self._postdam.use = 'server'
            self._postdam.send(nmsg, priority=1)

    def _status_needed(self):
        """ 
        Demanam l'estat a les antenes
        una per una.
        """    
        for i in range(8):
            self._postdam.use = 'antena_{}'.format(i)
            msg = dict(pid=i, type='get_status')
            self._postdam.send(msg, priority=5)
