#!/bin/bash

# Execute Beanstalk and server, and kill beanstalkd process
beanstalkd -l 127.0.0.1 -p 1300&
python2 Server.py
killall beanstalkd
