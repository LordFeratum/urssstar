#!/bin/bash

# Install Beanstalk Daemon
curl -L -k https://github.com/kr/beanstalkd/archive/master.tar.gz | tar zx
cd beanstalkd-master
make install
cd ..
rm -r beanstalkd-master

# Install Beanstalkc
curl -L -k https://github.com/earl/beanstalkc/archive/master.tar.gz | tar zx
cd beanstalkc-master
python2 setup.py install
cd ..
rm -r beanstalkd-master

# Execute program
./run.sh
